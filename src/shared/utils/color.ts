export const getColor = (hue: number, saturation = 100, lightness = 40, opacity = 1): string => {
  const hueRange = 360;

  return `hsla(${hue % hueRange}, ${saturation}%, ${lightness}%, ${opacity})`;
};
