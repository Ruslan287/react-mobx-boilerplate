# CLI

### Development
```js
npm i // in case you don't have node_modules installed
npm start
```

Go to http://localhost:4200/

### Production
```js
npm run build
```

In the `dist` folder will be the app built artifacts.

### Local preview build artifacts
```js
npm run preview
```

Go to http://localhost:4200/

### Testing
```js
npm run test
```

For Architecture description refer https://docs.google.com/presentation/d/1NBOWHmJ53HbToV1VylaETRSkd15hpLi5YUMeiJUy4jw/edit?usp=sharing